(function () {
  'use strict';

  self.addEventListener('notificationclick', (event) => {
    const action = event.notification;
    switch (action.actions[0].action) {
      case "explore":
        self.clients.openWindow(action.data.link)
        break;

      default:
        break;
    }

    event.notification.close();
    console.log('notification details: ', event.notification);
  });
}());
